resource "aws_iam_role" "lambda-role" {
  name = "ec2-start-stop-demo"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "lambda-ec2-role"
  }
}

resource "aws_iam_policy" "lambda-policy" {
  name = "lambda-ec2-start-stop-demo"

  policy = jsonencode({
    Version = "2012-10-17"
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        "Resource" : "arn:aws:logs:*:*:*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "ec2:DescribeInstances",
          "ec2:DescribeRegions",
          "ec2:StartInstances",
          "ec2:StopInstances"
        ],
        "Resource" : "*"
      }
    ]
  })
}
resource "aws_iam_role_policy_attachment" "lambda-ec2-policy-attach" {
  role       = aws_iam_role.lambda-role.name
  policy_arn = aws_iam_policy.lambda-policy.arn
}

resource "aws_lambda_function" "ec2-start-funaction" {
  filename      = "ec2-start.zip"
  function_name = "ec2-start-demo"
  role          = aws_iam_role.lambda-role.arn
  handler       = "ec2-start.lambda_handler"

  source_code_hash = filebase64sha256("ec2-start.zip")

  runtime = "python3.9"
  timeout = 63

  environment {
    variables = {
      key1 = "value1"
      key2 = "value2"
    }
  }
}

resource "aws_lambda_function" "ec2-stop-funaction" {
  filename      = "ec2-stop.zip"
  function_name = "ec2-stop-demo"
  role          = aws_iam_role.lambda-role.arn
  handler       = "ec2-stop.lambda_handler"

  source_code_hash = filebase64sha256("ec2-stop.zip")

  runtime = "python3.9"
  timeout = 63

  environment {
    variables = {
      key1 = "value1"
      key2 = "value2"
    }
  }
}

resource "aws_cloudwatch_event_rule" "ec2-start-rule" {
  name = "ec2-start-rule"
  description         = "Start EC2 instances on scheduled time"
  schedule_expression = "cron(12 05 ? * * *)"

}

resource "aws_cloudwatch_event_rule" "ec2-stop-rule" {
  name = "ec2-stop-rule"
  description         = "Stop EC2 instances on scheduled time"
  schedule_expression = "cron(20 05 ? * * *)"

}

resource "aws_cloudwatch_event_target" "lambda-start" {
  target_id = "lambda"
  rule      = aws_cloudwatch_event_rule.ec2-start-rule.name
  arn       = aws_lambda_function.ec2-start-funaction.arn
}

resource "aws_cloudwatch_event_target" "lambda-stop" {
  target_id = "lambda"
  rule      = aws_cloudwatch_event_rule.ec2-stop-rule.name
  arn       = aws_lambda_function.ec2-stop-funaction.arn

}

resource "aws_lambda_permission" "allow_start_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ec2-start-funaction.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.ec2-start-rule.arn
}

resource "aws_lambda_permission" "allow_stop_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ec2-stop-funaction.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.ec2-stop-rule.arn
}